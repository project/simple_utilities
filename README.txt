Simple Utilities
http://drupal.org/project/simple_utilities
================


DESCRIPTION
------------
A collection of inexpensive functions for pulling small amounts of data from core 
Drupal objects, nodes, users, files and comments. Sometimes you need to get a small 
piece of data, for example, the author id of a node, many times over in a page. 
Drupal's API provides node_load, which is great, but it can get a bit expensive 
if you have to call it hundreds of times.


REQUIREMENTS
------------
Drupal 7.x


INSTALLING
------------
1. Copy the 'simple_utilities' folder to your sites/all/modules directory.
2. Go to Administer > Site building > Modules. Enable the module.


CONFIGURING AND USING
---------------------
There is no admin interface. It just makes the functions available to the system.


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE
-----------------------------------------------------------
1. Go to the module issue queue at
   http://drupal.org/project/issues/simple_utilities?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to
   http://drupal.org/project/issues/user
5. When requesting new features please keep in mind that the entire purpose of this
   module is inexpensive functions. Complicated requests will not be considered.
