<?php
/**
 * @file
 * Provides node utility functions
 */
/**
 * Returns a node title
 *
 * @param $nid
 *   The node ID
 * @return
 *   The node title
 */
function simple_utilities_nodeTitle($nid) {
  $result = db_query("SELECT node.title AS node_title FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node author's user ID
 *
 * @param $nid
 *   The node ID
 * @return
 *   The user ID of the node's author
 */
function simple_utilities_nodeUid($nid) {
  $result = db_query("SELECT node.uid AS node_uid FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node's type
 *
 * @param $nid
 *   The node ID
 * @return
 *   The node type
 */
function simple_utilities_nodeType($nid) {
  $result = db_query("SELECT node.type AS node_type FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node's taxonomy
 *
 * @param $nid
 *   The node ID
 * @return
 *   An indexed array of objects of node taxamony data with the following properties
 *   'tid' = The term ID 
 *   'term_name' = The term name 
 *   'description' = The term description
 *   'weight' = The term weight
 *   'vid' = The vocabulary ID 
 *   'vocabulary_name' = The vocabulary name 
 */
function simple_utilities_nodeTaxonomy($nid) {
  $result = db_query("SELECT ttd.tid AS tid, ttd.vid AS vid, ttd.name AS term_name, ttd.description AS description, ttd.weight AS weight, vocab.name as vocabulary_name FROM {taxonomy_term_data} ttd INNER JOIN {taxonomy_index} ti ON ti.tid = ttd.tid INNER JOIN {taxonomy_vocabulary} vocab ON ttd.vid = vocab.vid WHERE ti.nid = :nid", array(':nid' => $nid))->fetchAll();
  return $result;
}

/**
 * Returns a node's published status
 *
 * @param $nid
 *   The node ID
 * @return
 *   The node status
 */
function simple_utilities_nodeStatus($nid) {
  $result = db_query("SELECT node.status AS node_status FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node's sticky property
 *
 * @param $nid
 *   The node ID
 * @return
 *   The node sticky property
 */
function simple_utilities_nodeSticky($nid) {
  $result = db_query("SELECT node.sticky AS node_sticky FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node's promoted to front page property
 *
 * @param $nid
 *   The node ID
 * @return
 *   The node promoted property
 */
function simple_utilities_nodePromoted($nid) {
  $result = db_query("SELECT node.promote AS node_promote FROM {node} node WHERE node.nid = :nid", array(':nid' => $nid))->fetchCol();
  return $result[0];
}

/**
 * Returns a node's last revision date and creation date.
 *
 * @param $nid
 *   The node ID
 * @return
 *   Object of timestamps with the following properties
 *   'created' = The creation date of the Node
 *   'changed' = The date of the last change
 */
function simple_utilities_nodeDates($nid) {
  $result = db_query("SELECT node_revision.timestamp AS changed, node.created AS created FROM {node} node LEFT JOIN {node_revision} node_revision ON node.vid = node_revision.vid WHERE node.nid = :nid", array(':nid' => $nid))->fetchAll();
  return $result[0];
}


/**
 * Returns a data on the node's last comment.
 *
 * @param $nid
 *   The node ID
 * @return
 *   An object with the following properties
 *   'cid' = Comment ID
 *   'timestamp' = Comment timestamp
 *   'uid' = Comment author user id,
 *   'username' = Comment author username
 *   'count' = The total number of comment on the node.
 */
function simple_utilities_nodeLastCommentData($nid) {
  $result = db_query("SELECT node_comment_statistics.comment_count AS count, node.comment AS cid, node_comment_statistics.last_comment_uid AS uid, node_comment_statistics.last_comment_timestamp AS timestamp, COALESCE(ncs_users.name, node_comment_statistics.last_comment_name) AS username FROM {node} node INNER JOIN {node_comment_statistics} node_comment_statistics ON node.nid = node_comment_statistics.nid LEFT JOIN {users} ncs_users ON node_comment_statistics.last_comment_uid = ncs_users.uid AND ncs_users.uid <> '0' WHERE node.nid = :nid", array(':nid' => $nid))->fetchAll();
  return $result[0];
}
