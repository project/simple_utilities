<?php
/**
 * @file
 * Provides comment  utility functions
 */
/**
 * Returns data on a comment
 *
 * @param $cid
 *   The comment ID
 * @return
 *  An object of comment data with the following properties
 *  title
 *  nid = The id of the node the comment is attached to
 *  status = approved or in moderation queue
 *  name = The comment author's name
 *  uid = The commnent author's user id
 *  homepage = The comment author's website
 *  hostname = Hostname of the user who posted the comment
 *  pid = The comment id of the it's parent comment
 *  thread = The depth of the comment if it's threaded
 *  created = Creation date timestamp
 *  body = The body of the comment
 */
function simple_utilities_commentData($cid) {
  $result = db_query("SELECT comment.subject AS title, comment.nid AS nid, comment.status AS status, comment.name AS name, comment.uid AS uid, comment.homepage AS homepage, comment.thread AS thread, comment.hostname AS hostname, comment.pid AS pid, comment.created AS created, 'comment' AS body FROM {comment} comment INNER JOIN {node} node_comment ON comment.nid = node_comment.nid WHERE comment.cid = :cid", array(':cid' => $cid))->fetchAll();
  return $result[0];
}