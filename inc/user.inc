<?php
/**
 * @file
 * Provides user utility functions
 */
/**
 * Returns a user name
 *
 * @param $uid
 *   The user ID as an integer or the user email as a string
 * @return
 *   The user name
 */
function simple_utilities_userName($arg) {
  if (is_int($arg)) {
    $sql = "SELECT users.name AS users_name FROM {users} users WHERE users.uid = :arg";
  } else {
    $sql = "SELECT users.name AS users_name FROM {users} users WHERE users.mail = :arg";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchCol();
  return $result[0];
}

/**
 * Returns a user status
 *
 * @param $uid
 *   The user ID as an integer or the user email as a string
 * @return
 *   The user status
 */
function simple_utilities_userStatus($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT users.status AS users_status FROM {users} users WHERE users.uid = :arg";
  } else {
  	$sql = "SELECT users.status AS users_status FROM {users} users WHERE users.mail = :arg";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchCol();
  return $result[0];
}

/**
 * Returns a user creation date
 *
 * @param $uid
 *   The user ID as an integer or the user email as a string
 * @return
 *   The user creation timestamp
 */
function simple_utilities_userCreationDate($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT users.created AS users_created FROM {users} users WHERE users.uid = :arg";
  } else {
  	$sql = "SELECT users.created AS users_created FROM {users} users WHERE users.mail = :arg";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchCol();
  return $result[0];
}

/**
 * Returns a user email
 *
 * @param $uid
 *   The user ID
 * @return
 *   The user email
 */
function simple_utilities_userEmail($uid) {
  $result = db_query("SELECT users.mail AS users_mail FROM {users} users WHERE users.uid = :uid", array(':uid' => $uid))->fetchCol();
  return $result[0];
}

/**
 * Returns a user ID
 *
 * @param $mail
 *   The user email address
 * @return
 *   The user ID
 */
function simple_utilities_userID($mail) {
  $result = db_query("SELECT users.uid AS users_uid FROM {users} users WHERE users.mail = :mail", array(':mail' => $mail))->fetchCol();
  return $result[0];
}

/**
 * Returns a user's last login
 *
 * @param $uid
 *   The user ID as an integer or the user email as a string
 * @return
 *   The user's last login
 */
function simple_utilities_userLogin($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT users.login AS users_login FROM {users} users WHERE users.uid = :arg";
  } else {
  	$sql = "SELECT users.login AS users_login FROM {users} users WHERE users.mail = :arg";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchCol();
  return $result[0];
}

/**
 * Returns a user's last access
 *
 * @param $arg
 *   The user ID as an integer or the user email as a string
 * @return
 *   The user's last access
 */
function simple_utilities_userAccess($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT users.access AS users_access FROM {users} users WHERE users.uid = :arg";
  } else {
  	$sql = "SELECT users.access AS users_access FROM {users} users WHERE users.mail = :arg";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchCol();
  return $result[0];
}



/**
 * Returns a user's picture data
 *
 * @param $uid
 * The user ID as an integer or the user email as a string
 * @return
 *   An object of file Data
 *   filename
 *   path_to_file = Path to File 
 *   mimetype
 *   size = in Bytes
 *   status
 *   timetamp = Upload date
 *   fid = File ID
 */
function simple_utilities_userPicture($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT users.picture AS users_picture FROM {users} users WHERE users.uid = :arg";
  } else {
  	$sql = "SELECT users.picture AS users_picture FROM {users} users WHERE users.mail = :arg";
  }
  $fid = db_query($sql, array(':arg' => $arg))->fetchCol();
  $result = simple_utilities_fileData($fid[0]);
  $result->fid = $fid[0];
  return $result;
}

/**
 * Returns a user's roles
 *
 * @param $uid
 *   The user ID as an integer or the user email as a string
 * @return
 *   An associative array of roles
 *   'role' = role name
 *   'rid' = role ID 
 *   'weight' = role weight
 */
function simple_utilities_userRoles($arg) {
  if (is_int($arg)) {
  	$sql = "SELECT role.name AS role, role.rid AS rid, role.weight AS weight FROM {role} role INNER JOIN {users_roles} users_roles ON role.rid = users_roles.rid WHERE users_roles.uid = :arg ORDER BY role.weight ASC";
  } else {
  	$sql = "SELECT role.name AS role, role.rid AS rid, role.weight AS weight FROM {role} role INNER JOIN {users_roles} users_roles ON role.rid = users_roles.rid WHERE users_roles.uid = (SELECT users.uid FROM {users} users WHERE users.mail = :arg) ORDER BY role.weight ASC";
  }
  $result = db_query($sql, array(':arg' => $arg))->fetchAll();
  return $result[0];
}