<?php
/**
 * @file
 * Provides file utility functions
 */
/**
 * Returns an file's data
 *
 * @param $fid
 *   The file ID
 * @return
 *   An array of file Data
 *   filename
 *   path_to_file = Path to File 
 *   mimetype
 *   size = in Bytes
 *   status
 *   timetamp = upload date
 */
function simple_utilities_fileData($fid) {
  $result = db_query("SELECT file_managed.filename AS filename, file_managed.uri AS path_to_file, file_managed.filemime AS mimetype, file_managed.filesize AS size, file_managed.status AS status, file_managed.timestamp AS timestamp FROM {file_managed} file_managed WHERE file_managed.fid = :fid", array(':fid' => $fid))->fetchAll();
  return $result[0];
}

/**
 * Returns an file's usage count
 *
 * @param $fid
 *   The file ID
 * @return
 *  The file's usage count
 */
function simple_utilities_fileUseCount($fid) {
  $result = db_query("SELECT file_usage.count AS count FROM {file_managed} file_managed LEFT JOIN {file_usage} file_usage ON file_managed.fid = file_usage.fid WHERE file_managed.fid = :fid", array(':fid' => $fid))->fetchCol();
  return $result[0];
}